﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class VideoAdManager : MonoBehaviour
{
    private static VideoAdManager _instance;
    
    public static VideoAdManager GetInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<VideoAdManager>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public enum WhichOneOfAds
    {
        MainMenuScene = 0, StoreScene = 1, OnFailSceneRewardVideo = 2, OnFailSceneInterstitalVideo = 3, OnPauseSceneRewardedVideo = 4, OnPauseSceneNoneRewardedVideo = 5, AccessoriesScene = 6, None = 7, 
    }

    public static WhichOneOfAds whichOneOfAds;

    public static Action<WhichOneOfAds> OnVideoPlayingDone = delegate { };
    public static Action OnVideoPlayingError = delegate { };
    public static Action OnVideoPlayingCancell = delegate { };

    void Start()
    {
        ConsoliAds.onRewardedVideoAdCompletedEvent += onRewardedVideoCompleted;
        //ConsoliAds.onAdCloseEvent += onAdCloseEvent;
        //ConsoliAds.onAdRequestFailedEvent += onAdRequestFailedEvent;
        //ConsoliAds.onAdShowFailedEvent += onAdShowFailedEvent;
        ConsoliAds.Instance.setAdNetworkQueueType(AdNetworkQueueType.RoundRobin);
        StartCoroutine("WaitToInitializingConsoliAds");
    }

    void Destroy()
    {
        ConsoliAds.onRewardedVideoAdCompletedEvent -= onRewardedVideoCompleted;
        //ConsoliAds.onAdCloseEvent -= onAdCloseEvent;
        //ConsoliAds.onAdRequestFailedEvent -= onAdRequestFailedEvent;
        //ConsoliAds.onAdShowFailedEvent -= onAdShowFailedEvent;
    }
    private void onAdShowFailedEvent()
    {
        Debug.Log("testtttttt: onAdShowFailedEvent");
        OnVideoPlayingError();
    }

    private void onAdRequestFailedEvent()
    {
        Debug.Log("testtttttt: onAdRequestFailedEvent");
        OnVideoPlayingError();
    }

    private void onAdCloseEvent()
    {
        Debug.Log("testtttttt: onAdCloseEvent");
        //LoadAds();
    }

    private void onRewardedVideoCompleted()
    {
        Debug.Log("testtttttt: onRewardedVideoCompleted");
        OnVideoPlayingDone(whichOneOfAds);
        whichOneOfAds = WhichOneOfAds.None;
        LoadAds();
    }

    IEnumerator WaitToInitializingConsoliAds()
    {
        yield return new WaitForSeconds(1f);
        LoadAds();
    }

    private void LoadAds()
    {
        ConsoliAds.Instance.LoadInterstitialForScene(0);
        ConsoliAds.Instance.LoadRewardedVideoForScene(1);
        ConsoliAds.Instance.LoadRewardedVideoForScene(2);
        ConsoliAds.Instance.LoadInterstitialForScene(2);
        ConsoliAds.Instance.LoadRewardedVideoForScene(3);
        ConsoliAds.Instance.LoadInterstitialForScene(3);
    }
    public void StartVideoAd(WhichOneOfAds whichOne)
    {
        whichOneOfAds = whichOne;
        switch (whichOne)
        {
            case WhichOneOfAds.MainMenuScene:
                ConsoliAds.Instance.ShowInterstitial(0);
                break;
            case WhichOneOfAds.StoreScene:
                ConsoliAds.Instance.ShowRewardedVideo(1);
                break;
            case WhichOneOfAds.AccessoriesScene:
                ConsoliAds.Instance.ShowRewardedVideo(1);
                break;
            case WhichOneOfAds.OnFailSceneInterstitalVideo:
                ConsoliAds.Instance.ShowInterstitial(2);
                break;
            case WhichOneOfAds.OnFailSceneRewardVideo:
                ConsoliAds.Instance.ShowRewardedVideo(2);
                break;
            case WhichOneOfAds.OnPauseSceneRewardedVideo:
                ConsoliAds.Instance.ShowRewardedVideo(3);
                break;
            case WhichOneOfAds.OnPauseSceneNoneRewardedVideo:
                ConsoliAds.Instance.ShowInterstitial(3);
                break;
        }
    }

    public bool HasVideoToShow(WhichOneOfAds whichOne)
    {
        switch (whichOne)
        {
            case WhichOneOfAds.MainMenuScene:
                return ConsoliAds.Instance.IsInterstitialAvailable(0);
            case WhichOneOfAds.StoreScene:
                return ConsoliAds.Instance.IsRewardedVideoAvailable(1);
            case WhichOneOfAds.OnFailSceneInterstitalVideo:
                return ConsoliAds.Instance.IsInterstitialAvailable(2);
            case WhichOneOfAds.OnFailSceneRewardVideo:
                return ConsoliAds.Instance.IsRewardedVideoAvailable(2);
            case WhichOneOfAds.OnPauseSceneRewardedVideo:
                return ConsoliAds.Instance.IsRewardedVideoAvailable(3);
            case WhichOneOfAds.OnPauseSceneNoneRewardedVideo:
                return ConsoliAds.Instance.IsInterstitialAvailable(3);
            default:
                return false;
        }
    }

    public void CheckForServicesAvailibity()
    {

    }
}
