﻿using UnityEngine;
using System.Collections;

public class ButtonClickManger : MonoBehaviour {

    // Main Menu Interstitial Ad
	public void OnBtnShowInterstitialMainMenu_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.MainMenuScene);
        Debug.Log("OnBtnShowInterstitialMainMenu_Click");
    }

    // Main menu rewarde video (free coin) in main menu's shop
    public void OnBtnShowRewardedVideoShopMainMenu_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.StoreScene);
        Debug.Log("OnBtnShowRewardedVideoShopMainMenu_Click");
    }

    // Main menu not enough coin in the Armory panel
    public void OnBtnShowRewardedVideoNotEnoughCoinManinMenu_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.AccessoriesScene);
        Debug.Log("OnBtnShowRewardedVideoNotEnoughCoinManinMenu_Click");
    }

    //When the player fails and end game panel showed (the final panel after revive panel)
    public void OnBtnShowInterstitialFailWave_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.OnFailSceneInterstitalVideo);
        Debug.Log("OnBtnShowInterstitialFailWave_Click");
    }

    // When the player wants to revive 
    public void OnBtnShowRewardedToRevive_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.OnFailSceneRewardVideo);
        Debug.Log("OnBtnShowRewardedToRevive_Click");
    }

    // When the player wants to watch rewarded video(free coin) in pause menu
    public void OnBtnShowRewardedInPauseMenu_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.OnPauseSceneRewardedVideo);
        Debug.Log("OnBtnShowRewardedInPauseMenu_Click");
    }

    // When pause button clicked
    public void OnBtnShowInterstitialPauseButtonClick_Click(){
        VideoAdManager.GetInstance.StartVideoAd(VideoAdManager.WhichOneOfAds.OnPauseSceneNoneRewardedVideo);
        Debug.Log("OnBtnShowInterstitialPauseButtonClick_Click");
    }
}
