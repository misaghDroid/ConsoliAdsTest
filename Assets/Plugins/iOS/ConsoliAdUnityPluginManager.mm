//
//  ConsoliAdUnityPluginManager.m
//  test
//
//  Created by FazalElahi on 06/02/2017.
//  Copyright © 2017 FazalElahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConsoliAdUnityPluginManager.h"
#import "ConsoliAdUnityPlugin.h"

#if UNITY_IOS
void UnitySendMessage(const char *className, const char *methodName, const char *param);
#endif

@implementation ConsoliAdUnityPluginManager

+ (BOOL)initWithKey:(NSString*)appKey andDeviceID:(NSString*)deviceID andGameObject:(NSString*)gameObjectName {
   return [[ConsoliAdUnityPlugin sharedPlugIn] initWithKey:appKey andDeviceID:deviceID andGameObject:gameObjectName];
}

+ (BOOL)showInterstitial:(int)scene {
    return [[ConsoliAdUnityPlugin sharedPlugIn] showInterstitial:scene];
}

+ (void)loadInterstitialForScene:(int)scene {
    [[ConsoliAdUnityPlugin sharedPlugIn] loadInterstitialForScene:scene];
}

+ (void)sendStatsOnPauseWithDeviceID:(NSString*)deviceID {
    [[ConsoliAdUnityPlugin sharedPlugIn] processStatsOnPauseWithDeviceID:deviceID];
}

+ (void)sendMessageToUnity:(NSString*)gameObjectName method:(NSString*)methodName location:(NSString*)location {
#if UNITY_IOS
    UnitySendMessage([gameObjectName UTF8String], [methodName UTF8String], [location UTF8String]);
#endif
}

extern "C" {
    
    bool _initAppWithKey(char *appKey,char *deviceID,char* gameObjectName)
    {
        return [ConsoliAdUnityPluginManager initWithKey:[NSString stringWithUTF8String:appKey] andDeviceID:[NSString stringWithUTF8String:deviceID] andGameObject:[NSString stringWithUTF8String:gameObjectName]];
    }
    
    bool _showInterstitial(int scene)
    {
        return [ConsoliAdUnityPluginManager showInterstitial:scene];
    }
    
    void _loadInterstitialForScene(int scene)
    {
        [ConsoliAdUnityPluginManager loadInterstitialForScene:scene];
    }
    void _sendStatsOnPauseWithDeviceID(char *deviceID)
    {
        return [ConsoliAdUnityPluginManager sendStatsOnPauseWithDeviceID:[NSString stringWithUTF8String:deviceID]];
    }
}

@end
