﻿using System;
using UnityEngine;
using System.Collections.Generic;
public class AdNetworkFactory
{
    public static string amRateUsURL = "http://www.amazon.com/gp/mas/dl/android?p=";
    public static string gpRateUsURL = "https://play.google.com/store/apps/details?id=" ;

    public string moreFunURL;
    public string rateUsURL;
    public string admobBannerID { get; private set; }
    private string admobInterstitialID;
    private string admobRewardedVideoID;
    public string admobNativeAdID { get; private set; }
    private string heyzapID;
    private string chartboostID;
    private string chartboostSignature;
    private string adColonyAppID;
    private string adColonyZoneID;
    private string unityAdsID;
    private string leadboltAppKey;
    private string supersonicAppKey;
    private string appLovinID;
    private string consoliadsAppKey;
	private string tapJoyAdsID;
	private string tapJoyAdsPlacement;
	private string vungleAdsID;
	private string mobvistaAppKey;
	private string mobvistaAppID;
	private string mvInterstitialUnitID;
	private string mvVideoUnitID;
	private string facebookInterstitialId;
	private string facebookRewardedId;

    private Dictionary<String, String> revmobIds;

    public AdNetwork getAdNetworkInstance(AdNetworkType type)
    {
        AdNetwork adNetwork;
        switch (type)
        {
		case AdNetworkType.CHARTBOOST:
			{
				Type adNetworkType = Type.GetType ("CAChartBoost");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = chartboostID;
					adNetwork.appSignature = chartboostSignature;

					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;

				} else {
					return null;
				}
			}
		case AdNetworkType.CHARTBOOSTMOREAPPS:
			{
				Type adNetworkType = Type.GetType ("CAChartBoostMoreApps");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = chartboostID;
					adNetwork.appSignature = chartboostSignature;

					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;

				} else {
					return null;
				}
			}
		case AdNetworkType.CHARTBOOSTREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAChartBoostRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = chartboostID;
					adNetwork.appSignature = chartboostSignature;

					adNetwork.type = type;
					adNetwork.isRewardedAd = true;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;

				} else {
					return null;
				}
			}
		case AdNetworkType.REVMOBFULLSCREEN:
			{
				Type adNetworkType = Type.GetType ("CARevmobFullScreen");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appIds = revmobIds;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.REVMOBVIDEO:
			{
				Type adNetworkType = Type.GetType ("CARevmobVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appIds = revmobIds;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.REVMOBREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CARevmobRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appIds = revmobIds;
					adNetwork.type = type;
					adNetwork.isRewardedAd = true;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
        
		case AdNetworkType.ADCOLONY:
			{
				Type adNetworkType = Type.GetType ("CAAdColony");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = adColonyAppID;
					adNetwork.adColonyZoneID = adColonyZoneID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else
				{
					return null;
				}
			}
		case AdNetworkType.TAPJOYADS:
			{
				Type adNetworkType = Type.GetType ("CATapJoyAds");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = tapJoyAdsID;
					adNetwork.tapJoyPlacementName = tapJoyAdsPlacement;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				}
				else
				{
					return null;
				}
			}
		case AdNetworkType.ADMOBINTERSTITIAL:
			{
				adNetwork = new CAAdmobInterstitial();
				adNetwork.appKey = admobInterstitialID;
				adNetwork.type = type;
				adNetwork.hasClickCallback = true;
				adNetwork.hasRequestCallback = true;
				return adNetwork;
			}
		case AdNetworkType.ADMOBREWARDEDVIDEO:
			{
				adNetwork = new CAAdmobRewardedVideo();
				adNetwork.appKey = admobRewardedVideoID;
				adNetwork.isRewardedAd = true;
				adNetwork.type = type;
				adNetwork.hasClickCallback = true;
				adNetwork.hasRequestCallback = true;
				return adNetwork;
			}
		case AdNetworkType.LEADBOLTINTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CALeadboltInterstitial");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = leadboltAppKey;
					adNetwork.type = type;

					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.LEADBOLTREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CALeadboltRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = leadboltAppKey;
					adNetwork.isRewardedAd = true;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.HEYZAPINTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CAHeyzapInterstitial");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = heyzapID;
					adNetwork.type = type;

					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.HEYZAPVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAHeyzapVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = heyzapID;
					adNetwork.type = type;

					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}

			}

		case AdNetworkType.UNITYADS:
			{
				Type adNetworkType = Type.GetType ("CAUnityAds");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = unityAdsID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = false;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.SUPERSONICINTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CASupersonicInterstitial");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = supersonicAppKey;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.SUPERSONICOFFERWALL:
			{
				Type adNetworkType = Type.GetType ("CASupersonicOfferwall");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = supersonicAppKey;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = false;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.SUPERSONICREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CASupersonicRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = supersonicAppKey;
					adNetwork.isRewardedAd = true;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.APPLOVININTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CAApplovinInterstitial");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = appLovinID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.APPLOVINREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAApplovinRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = appLovinID;
					adNetwork.isRewardedAd = true;
					adNetwork.type = type;

					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.UNITYADSREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAUnityAdsRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = unityAdsID;
					adNetwork.isRewardedAd = true;
					adNetwork.type = type;
					adNetwork.hasClickCallback = false;
					adNetwork.hasRequestCallback = false;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.CONSOLIADS:
			{
				adNetwork = new CAInterstitial();
				adNetwork.appKey = consoliadsAppKey;
				adNetwork.type = type;
				adNetwork.hasClickCallback = true;
				adNetwork.hasRequestCallback = false;
				return adNetwork;
			}
		case AdNetworkType.VUNGLEADS:
			{
				Type adNetworkType = Type.GetType ("CAVungleAds");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = vungleAdsID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = false;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.MOBVISTAINTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CAMVInterstitial");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = mobvistaAppKey;
					adNetwork.mobvistaAppID = mobvistaAppID;
					adNetwork.mobvistaAdUitID = mvInterstitialUnitID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}
		case AdNetworkType.MOBVISTAREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAMVRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = mobvistaAppKey;
					adNetwork.mobvistaAppID = mobvistaAppID;
					adNetwork.mobvistaAdUitID = mvVideoUnitID;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}


		case AdNetworkType.FACEBOOKINTERSTITIAL:
			{
				Type adNetworkType = Type.GetType ("CAFacebookInterstitial");
				Debug.Log ("FB AD network..." + adNetworkType);
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = facebookInterstitialId;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

		case AdNetworkType.FACEBOOKREWARDEDVIDEO:
			{
				Type adNetworkType = Type.GetType ("CAFacebookRewardedVideo");
				if (adNetworkType != null) {
					adNetwork = (AdNetwork)Activator.CreateInstance (adNetworkType);
					adNetwork.appKey = facebookRewardedId;
					adNetwork.type = type;
					adNetwork.hasClickCallback = true;
					adNetwork.hasRequestCallback = true;
					return adNetwork;
				} else {
					return null;
				}
			}

        }
        return null;
    }
    public void setupAdNetworkIDs()
    {
#if UNITY_ANDROID
        if (ConsoliAds.Instance.platform == Platform.Amazon)
        {
            rateUsURL = ConsoliAds.Instance.adIDList.gpRateUsURL;
            moreFunURL = rateUsURL + "&showAll=1";
            admobBannerID = ConsoliAds.Instance.adIDList.amAdmobBannerAdUnitID;
            admobInterstitialID = ConsoliAds.Instance.adIDList.amAdmobInterstitialAdUnitID;
            admobRewardedVideoID = ConsoliAds.Instance.adIDList.amAdmobRewardedVideoAdUnitID;
            admobNativeAdID = ConsoliAds.Instance.adIDList.amAdmobNativeAdUnitID;
            leadboltAppKey = ConsoliAds.Instance.adIDList.amLeadboltAppKey;
            heyzapID = ConsoliAds.Instance.adIDList.amHeyzapID;
            chartboostID = ConsoliAds.Instance.adIDList.amChartboostAppID;
            chartboostSignature = ConsoliAds.Instance.adIDList.amChartboostAppSignature;
            adColonyAppID = ConsoliAds.Instance.adIDList.amAdcolonyAppID;
            adColonyZoneID = ConsoliAds.Instance.adIDList.amAdcolonyZoneID;
            revmobIds = new Dictionary<String, String>() {
                           { "Android", ConsoliAds.Instance.adIDList.amRevmobMediaID},
                        };
            unityAdsID = ConsoliAds.Instance.adIDList.amUnityadsAppID;
            supersonicAppKey = ConsoliAds.Instance.adIDList.amSupersonicAppKey;
            appLovinID = ConsoliAds.Instance.adIDList.amApplovinID;
            consoliadsAppKey = ConsoliAds.Instance.adIDList.amConsoliadsAppKey;
			tapJoyAdsID = ConsoliAds.Instance.adIDList.amTapJoyID;
			tapJoyAdsPlacement = ConsoliAds.Instance.adIDList.amTapJoyPlacement;
			vungleAdsID = ConsoliAds.Instance.adIDList.amVungleID;
			mobvistaAppKey = ConsoliAds.Instance.adIDList.amMobVistaAppKey;
			mobvistaAppID = ConsoliAds.Instance.adIDList.amMobVistaAppID;
			mvInterstitialUnitID = ConsoliAds.Instance.adIDList.amMobVistaInterstitialID;
			mvVideoUnitID = ConsoliAds.Instance.adIDList.amMobVistaVideoID;
			facebookInterstitialId = ConsoliAds.Instance.adIDList.amFacebookInterstitialUnitId;
			facebookRewardedId = ConsoliAds.Instance.adIDList.amFacebookRewardedUnitId;
        }
        else
        {
            moreFunURL = ConsoliAds.Instance.adIDList.gpMoreAppsURL;
            //rateUsURL = gpRateUsURL + ConsoliAds.Instance.bundleIdentifier;
            rateUsURL = ConsoliAds.Instance.adIDList.gpRateUsURL;
            admobBannerID = ConsoliAds.Instance.adIDList.gpAdmobBannerAdUnitID;
            admobInterstitialID = ConsoliAds.Instance.adIDList.gpAdmobInterstitialAdUnitID;
            admobRewardedVideoID = ConsoliAds.Instance.adIDList.gpAdmobRewardedVideoAdUnitID;
            admobNativeAdID = ConsoliAds.Instance.adIDList.gpAdmobNativeAdUnitID;
            leadboltAppKey = ConsoliAds.Instance.adIDList.gpLeadboltAppKey;
            heyzapID = ConsoliAds.Instance.adIDList.gpHeyzapID;
            chartboostID = ConsoliAds.Instance.adIDList.gpChartboostAppID;
            chartboostSignature = ConsoliAds.Instance.adIDList.gpChartboostAppSignature;
            adColonyAppID = ConsoliAds.Instance.adIDList.gpAdcolonyAppID;
            adColonyZoneID = ConsoliAds.Instance.adIDList.gpAdcolonyZoneID;
            revmobIds = new Dictionary<String, String>() {
                           { "Android", ConsoliAds.Instance.adIDList.gpRevmobMediaID},
                        };
            unityAdsID = ConsoliAds.Instance.adIDList.gpUnityadsAppID;
            supersonicAppKey = ConsoliAds.Instance.adIDList.gpSupersonicAppKey;
            appLovinID = ConsoliAds.Instance.adIDList.gpApplovinID;
            consoliadsAppKey = ConsoliAds.Instance.adIDList.gpConsoliadsAppKey;
			tapJoyAdsID = ConsoliAds.Instance.adIDList.gpTapJoyID;
			tapJoyAdsPlacement = ConsoliAds.Instance.adIDList.gpTapJoyPlacement;
			vungleAdsID = ConsoliAds.Instance.adIDList.gpVungleID;
			mobvistaAppKey = ConsoliAds.Instance.adIDList.gpMobVistaAppKey;
			mobvistaAppID = ConsoliAds.Instance.adIDList.gpMobVistaAppID;
			mvInterstitialUnitID = ConsoliAds.Instance.adIDList.gpMobVistaInterstitialID;
			mvVideoUnitID = ConsoliAds.Instance.adIDList.gpMobVistaVideoID;
			facebookInterstitialId = ConsoliAds.Instance.adIDList.gpFacebookInterstitialUnitId;
			facebookRewardedId = ConsoliAds.Instance.adIDList.gpFacebookRewardedUnitId;

        }
#elif UNITY_IPHONE
        moreFunURL = ConsoliAds.Instance.adIDList.asMoreAppsURL;
        rateUsURL = ConsoliAds.Instance.adIDList.asRateUsURL;
        admobBannerID = ConsoliAds.Instance.adIDList.asAdmobBannerAdUnitID;
        admobInterstitialID = ConsoliAds.Instance.adIDList.asAdmobInterstitialAdUnitID;
        admobRewardedVideoID = ConsoliAds.Instance.adIDList.asAdmobRewardedVideoAdUnitID;
        admobNativeAdID = ConsoliAds.Instance.adIDList.asAdmobNativeAdUnitID;
        leadboltAppKey = ConsoliAds.Instance.adIDList.asLeadboltAppKey;
        heyzapID = ConsoliAds.Instance.adIDList.asHeyzapID;
        chartboostID = ConsoliAds.Instance.adIDList.asChartboostAppID;
        chartboostSignature = ConsoliAds.Instance.adIDList.asChartboostAppSignature;
        adColonyAppID = ConsoliAds.Instance.adIDList.asAdcolonyAppID;
        adColonyZoneID = ConsoliAds.Instance.adIDList.asAdcolonyZoneID;
        revmobIds = new Dictionary<String, String>() {
                               { "IOS", ConsoliAds.Instance.adIDList.asRevmobMediaID},
                            };
        unityAdsID = ConsoliAds.Instance.adIDList.asUnityadsAppID;
        supersonicAppKey = ConsoliAds.Instance.adIDList.asSupersonicAppKey;
        appLovinID = ConsoliAds.Instance.adIDList.asApplovinID;
        consoliadsAppKey = ConsoliAds.Instance.adIDList.asConsoliadsAppKey;
		tapJoyAdsID = ConsoliAds.Instance.adIDList.asTapJoyID;
		tapJoyAdsPlacement = ConsoliAds.Instance.adIDList.asTapJoyPlacement;
		vungleAdsID = ConsoliAds.Instance.adIDList.asVungleID;
		mobvistaAppKey = ConsoliAds.Instance.adIDList.asMobVistaAppKey;
		mobvistaAppID = ConsoliAds.Instance.adIDList.asMobVistaAppID;
		mvInterstitialUnitID = ConsoliAds.Instance.adIDList.asMobVistaInterstitialID;
		mvVideoUnitID = ConsoliAds.Instance.adIDList.asMobVistaVideoID;
		facebookInterstitialId = ConsoliAds.Instance.adIDList.asFacebookInterstitialUnitId;
		facebookRewardedId = ConsoliAds.Instance.adIDList.asFacebookRewardedUnitId;
#endif
    }


}
