﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class CAAdColony : AdNetwork
{

   // public string adColonyZoneID;
	AdColony.InterstitialAd Ad = null;
	bool isAdInitialized = false;
	bool isAdAvailable = false;

    public override void initialize(string gameObjectName, string uniqueDeviceID)
    {
		AdColony.Ads.OnConfigurationCompleted += OnConfigurationCompleted;
		AdColony.Ads.OnRequestInterstitial += OnRequestInterstitial;
		AdColony.Ads.OnRequestInterstitialFailedWithZone += OnRequestInterstitialFailedWithZone;
		AdColony.Ads.OnOpened += OnOpened;
		AdColony.Ads.OnClosed += OnClosed;
		AdColony.Ads.OnExpiring += OnExpiring;

		AdColony.AppOptions appOptions = new AdColony.AppOptions();
		appOptions.AdOrientation = AdColony.AdOrientationType.AdColonyOrientationAll;
		string[] zoneIDs = new string[] {adColonyZoneID, adColonyZoneID };
		AdColony.Ads.Configure(appKey, null, zoneIDs);
    }

	void RequestAd() {
		// Request an ad.
		//Debug.Log("**** Request Ad ****");

		AdColony.AdOptions adOptions = new AdColony.AdOptions();
		adOptions.ShowPrePopup = false;
		adOptions.ShowPostPopup = false;

		AdColony.Ads.RequestInterstitialAd(adColonyZoneID, adOptions);
	}
	public override bool showAd(int sceneID)
	{
		if (Ad != null && isAdAvailable == true) {
			AdColony.Ads.ShowAd(Ad);
			return true;
		}
		return false;
	}

	public override bool IsAdAvailable(int sceneID)
	{
		return isAdAvailable;
	}

	private void OnAdColonyVideoFinished(bool ad_was_shown)
	{
		if (ad_was_shown)
		{
			ConsoliAds.Instance.onVideoAdShown(type);

		}
	}
	public void AdAvailabilityChangeDelegate(bool available, string zoneId)
	{
		if (available)
		{
			ConsoliAds.Instance.onAdRequested(type);
		}
		else
		{
			ConsoliAds.Instance.onAdRequestFailed(type);
		}
	}

	public void OnConfigurationCompleted(List<AdColony.Zone> zones_) {
	
		//Debug.Log("AdColony.Ads.OnConfigurationCompleted called");

		if (zones_ != null && zones_.Count > 0) {
			isAdInitialized = true;
			//Debug.Log("zone count " + zones_.Count);
			RequestAd();
		}
	}

	public void OnRequestInterstitial(AdColony.InterstitialAd ad_) {
		//Debug.Log("AdColony.Ads.OnRequestInterstitial called");
		Ad = ad_;
		isAdAvailable = true;
		ConsoliAds.Instance.onAdRequested(type);
	}

	public void OnRequestInterstitialFailedWithZone(string zoneId) {

		//Debug.Log("AdColony.Ads.OnRequestInterstitialFailedWithZone called, zone: " + zoneId);
		isAdAvailable = false;
		ConsoliAds.Instance.onAdRequestFailed(type);
	}

	public void OnOpened(AdColony.InterstitialAd ad_) {
		//Debug.Log("AdColony.Ads.OnOpened called");
		isAdAvailable = false;
		RequestAd();
	}

	public void OnClosed(AdColony.InterstitialAd ad_) {
		//Debug.Log("AdColony.Ads.OnClosed called, expired: " + ad_.Expired);
		isAdAvailable = false;
		ConsoliAds.Instance.onVideoAdShown(type);
		RequestAd();
	}

	public void OnExpiring(AdColony.InterstitialAd ad_) {
		//Debug.Log("AdColony.Ads.OnExpiring called");
		Ad = null;
		isAdAvailable = false;

	}

}
