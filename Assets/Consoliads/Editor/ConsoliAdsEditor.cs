using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using SimpleJSON;
using System;

[CustomEditor(typeof(ConsoliAds))]
public class ConsoliAdsEditor : Editor
{
	string missMatchStr = "";
    public override void OnInspectorGUI()
    {
		
        DrawDefaultInspector();


        ConsoliAds sdkScript = (ConsoliAds)target;


        /*
        for (int i = 0; i < sdkScript.sceneList.Length; i++)
        {
            SceneTypes type = sdkScript.sceneList[i].sceneType;
            if (type != SceneTypes.RewardedVideo)
            {
                //is a rewarded video

                for (int j = 0; j < sdkScript.sceneList[i].adNetworkList.Length; j++)
                {
                    if (sdkScript.sceneList[i].adNetworkList[j] == AdNetworkType.ADMOBREWARDEDVIDEO)
                    {
                        sdkScript.sceneList[i].adNetworkList[j] = AdNetworkType.EMPTY;
                        EditorUtility.DisplayDialog("Error", "Can not select rewarded video ads in this scene", "Ok");
                    }
                }
            }
            else
            {

            }
        }
        */

        if (GUILayout.Button("Configure Server"))
        {
			String applicationIdentifier = "";
			#if UNITY_5_6_OR_NEWER
			applicationIdentifier = PlayerSettings.applicationIdentifier;
			#else
			applicationIdentifier = PlayerSettings.bundleIdentifier;
			#endif
	
            string result = null;
            //sdkScript.ConfigureServer();
            String errorMsg = "", warnings = "";
            if (sdkScript.userSignature == "")
            {
                errorMsg += "User Signature cannot be empty!\n";
            }

            if (sdkScript.productName == "")
            {
                errorMsg += "Product Name cannot be empty!\n";
            }
            if (sdkScript.bundleIdentifier == "")
            {
                errorMsg += "Bundle Identifier cannot be empty!";
            }
            if (Platform.IsDefined(typeof(Platform), sdkScript.platform) == false)
            {
                errorMsg += "Store cannot be empty!";
            }
			if (sdkScript.platform != Platform.Apple && EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android ) 
			{
				errorMsg += "Plateform does not mactch with your Target Plateform!\n";
			}
			else if (sdkScript.platform == Platform.Apple && EditorUserBuildSettings.activeBuildTarget != BuildTarget.iOS)
			{
				errorMsg += "Plateform does not mactch with your Target Plateform!\n";
			}
			if (applicationIdentifier != sdkScript.bundleIdentifier)
            {
                warnings += "Bundle Indentifier does not match with your application's bundle indentifier!\n";
            }

            if (errorMsg != "")
            {
                EditorUtility.DisplayDialog("Error", errorMsg, "Ok");
            }
            else {

				JSONClass allAdNetworks = validateAllScenesAdNetwork (sdkScript);
				if (missMatchStr != "") {
					EditorUtility.DisplayDialog("Alert", "Following Ad Networks are not integrated, either remove them from scenes or integrate them in " +
						"ConsoliAds before clicking \"Configure Server\" \nFor more info Kindly see the documentation on how to add or remove AdNetworks from ConsoliAdsPlugin " + "\n" + missMatchStr, "Ok");
					return;
				}

                if (warnings != "")
                {
                    bool dialogResult = EditorUtility.DisplayDialog("Warning", warnings, "Continue", "Cancel");
                    if (dialogResult)
                    {
						result = ServerConfig.Instance.configureServer(sdkScript,allAdNetworks);
                    }
                    else {
                        EditorUtility.DisplayDialog("Success", "ConsoliAds synchronization was canceled", "Ok");
                    }
                }
                else {

					result = ServerConfig.Instance.configureServer(sdkScript,allAdNetworks);
                }
            }
            if (result != null)
            {
                //popup that show the response message from server
                EditorUtility.DisplayDialog("Configure server", result, "Ok");
            }
        }


        if (GUILayout.Button("Goto Consoli Ads"))
        {
			Help.BrowseURL("https://portal.consoliads.com");
        }
    }
	public JSONClass validateAllScenesAdNetwork(ConsoliAds CAInstance)
	{
		List<int> exileAdNetworkList = new List<int>();

		for (int sequenceCounter = 0; sequenceCounter < CAInstance.sceneList.Length; sequenceCounter++)
		{
			for (int adCounter = 0; adCounter < CAInstance.sceneList[sequenceCounter].interstitialAndVideo.networkList.Length; adCounter++)
			{
				AdNetworkTypeInterstitialAndVideo ad = CAInstance.sceneList[sequenceCounter].interstitialAndVideo.networkList[adCounter];
				if (ad != AdNetworkTypeInterstitialAndVideo.EMPTY) {
					exileAdNetworkList.Add ((int)ad);
				}
			}

			for (int adCounter = 0; adCounter < CAInstance.sceneList[sequenceCounter].rewardedVideo.networkList.Length; adCounter++)
			{
				AdNetworkTypeRewardedVideo ad = CAInstance.sceneList[sequenceCounter].rewardedVideo.networkList[adCounter];
				if (ad != AdNetworkTypeRewardedVideo.EMPTY) {
					exileAdNetworkList.Add ((int)ad);
				}
			}

            AdNetworkType failOverInterstitial = (AdNetworkType)CAInstance.sceneList[sequenceCounter].interstitialAndVideo.failOver;
            if (failOverInterstitial != AdNetworkType.EMPTY) {
                    exileAdNetworkList.Add ((int)failOverInterstitial);
                }
            AdNetworkType failOverRewarded = (AdNetworkType)CAInstance.sceneList[sequenceCounter].rewardedVideo.failOver;
            if (failOverRewarded != AdNetworkType.EMPTY) {
                    exileAdNetworkList.Add ((int)failOverRewarded);
                }

		}
		ArrayList arraylist = AdNetwork.getIntegratedAdNetworksList(exileAdNetworkList);
		List<int> mismatchedAdnetwork = (List<int>)arraylist[0];

		missMatchStr = "";
		foreach (int element in mismatchedAdnetwork) {
			missMatchStr = missMatchStr + "\n" + (AdNetworkType)element;
		}
		JSONClass allAdNetworks = (JSONClass)arraylist[1];
		return allAdNetworks;
	}

}

