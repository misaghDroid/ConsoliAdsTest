﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SampleNew : MonoBehaviour {

	private bool isInitialized = false;
	private bool isNeedToUpdate = true;

	[Header("Texts:")]
	public Text adNetworkListText;

	[Header("Buttons:")]
	public Button showAdButton;

	[Header("Toggles:")]
	public Toggle RoundRobinToggle;
	public Toggle PriorityToggle;

	[Header("DropDown:")]
	public Dropdown AdTypeDropDown;
	public Dropdown SceneNameDropDown;

	public List<string> AdTypeList = new List<string>();
	public List<string> SceneNameList = new List<string>();

	void Start () {

		adNetworkListText.text = "";

		AdTypeList.Add ("Interstitial/Video");
		AdTypeList.Add ("Rewarded");
		AdTypeList.Add ("Banner");
		AdTypeList.Add ("Native");

		AdTypeDropDown.options.Clear ();
		//fill the dropdown menu OptionData with all COM's Name in ports[]
		foreach (string c in AdTypeList) 
		{
			AdTypeDropDown.options.Add (new Dropdown.OptionData() {text=c});
		}
		//this swith from 1 to 0 is only to refresh the visual DdMenu
		AdTypeDropDown.value = 1;
		AdTypeDropDown.value = 0;

		SceneNameDropDown.options.Clear ();
		//SceneNameDropDown.value = 0;
	}

	void OnEnable()
	{
		SetupEvents();
	}
	void SetupEvents()
	{
		// Listen to all impression-related events
		ConsoliAds.onInterstitialAdShownEvent += onInterstitialAdShown;
		ConsoliAds.onVideoAdShownEvent += onVideoAdShown;
		ConsoliAds.onRewardedVideoAdShownEvent += onRewardedVideoAdShown;
		ConsoliAds.onPopupAdShownEvent += onPopupAdShown;
		ConsoliAds.onRewardedVideoAdCompletedEvent += onRewardedVideoCompleted;
		ConsoliAds.onConsoliAdsInitialization += onConsoliAdsInitialization;

	}

	void Update () {

		if (isInitialized) {

			if (isNeedToUpdate) {

				isNeedToUpdate = false;
			
				Debug.Log ("isInitialized_New " + isInitialized + " " + isNeedToUpdate);

				SceneNameDropDown.options.Clear ();

				foreach (CAScene scene in ConsoliAds.Instance.sceneList) 
				{
					SceneNameList.Add ("" + scene.sceneType);
					SceneNameDropDown.options.Add (new Dropdown.OptionData() {text= "" + scene.sceneType});
				}
				if (ConsoliAds.Instance.sceneList.Length > 1) {
					SceneNameDropDown.value = 1;
					SceneNameDropDown.value = 0;
				}
				else {
					SceneNameDropDown.value = 0;
				}

			}
		}

	}

	public void updateAdNetworkText(int sceneID){
	
		if (sceneID >= ConsoliAds.Instance.sceneList.Length) {
			return;
		}
		CAScene scene = ConsoliAds.Instance.sceneList [sceneID];
		//adNetworkListText.text = "AdNetworks: \n";
		string str = "AdNetworks: \n";

		switch (AdTypeDropDown.value) {
		case 0:
			{
				foreach (AdNetworkType adNetwork in scene.interstitialAndVideo.networkList ) 
				{
					str += "" + adNetwork + "\n";
					Debug.Log("adNetwork " + adNetwork + " adNetworkListText " + str);
				}
			}
			break;
		case 1:
			{
				foreach (AdNetworkType adNetwork in scene.rewardedVideo.networkList ) 
				{
					str += "" + adNetwork + "\n";
				}
			}
			break;
		case 2:
			{
				if (scene.banner.enabled) {
					adNetworkListText.text = adNetworkListText.text + "Admob Banner" + "\n";
				}
			}
			break;
		case 3:
			{
				if (scene.native.enabled) {
					adNetworkListText.text = adNetworkListText.text + "Admob Native" + "\n";
				}
			}
			break;
		}
		adNetworkListText.text = str;
		Debug.Log ("Final text " + str);
	}

	public void roundRobinToggleValueChange() {

		PriorityToggle.isOn = !RoundRobinToggle.isOn;
		if(RoundRobinToggle.isOn){
			ConsoliAds.Instance.setAdNetworkQueueType (AdNetworkQueueType.RoundRobin);
		}
	}

	public void priorityToggleValueChange() {

		RoundRobinToggle.isOn = !PriorityToggle.isOn;
		if(PriorityToggle.isOn){
			ConsoliAds.Instance.setAdNetworkQueueType (AdNetworkQueueType.RoundRobin);
		}
	}
		
	public void adTypeDropDownValueChanged(int value) {

		if (isInitialized){
			updateAdNetworkText (SceneNameDropDown.value);
		}
		else {
			Debug.Log ("Consoliads is not Initialized");
		}
	}

	public void sceneNameDropDownValueChanged(int value) {
		if(isInitialized){
			updateAdNetworkText (SceneNameDropDown.value);
		}
		else {
			Debug.Log ("Consoliads is not Initialized");
		}
	}

	public void showAd()
	{
		Debug.Log ("showAd called " + SceneNameDropDown.value);
		if (isInitialized) {

			switch (AdTypeDropDown.value) {
			case 0:
				{
					ConsoliAds.Instance.ShowInterstitial(SceneNameDropDown.value);
				}
				break;
			case 1:
				{
					ConsoliAds.Instance.ShowRewardedVideo(SceneNameDropDown.value);
				}
				break;
			case 2:
				{
					ConsoliAds.Instance.ShowBanner(SceneNameDropDown.value);
				}
				break;
			case 3:
				{
					ConsoliAds.Instance.LoadNativeAd(SceneNameDropDown.value);
				}
				break;
			}
		}
		else {
			Debug.Log ("Consoliads is not Initialized");
		}

	}


	/* ------------------------ */

	void onConsoliAdsInitialization()
	{
		isInitialized = true;
		Debug.Log("Sample: onConsoliAdsInitialization called ");
	}

	void onInterstitialAdShown()
	{
		Debug.Log("Sample: onInterstitialAdShown called");
	}
	void onVideoAdShown()
	{
		Debug.Log("Sample: onVideoAdShown called");
	}
	void onRewardedVideoAdShown()
	{
		Debug.Log("Sample: onRewardedVideoAdShown called");
	}
	void onPopupAdShown()
	{
		Debug.Log("Sample: onPopupAdShown called");
	}
	public void onRewardedVideoCompleted()
	{
		Debug.Log("Sample: Event received : Rewarded Video Complete");
	}
}
